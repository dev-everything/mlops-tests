# export MLFLOW_TRACKING_URI="https://gitlab.com/api/v4/projects/56492185/ml/mlflow"
# export MLFLOW_TRACKING_TOKEN="glpat-..."
import mlflow

client = mlflow.tracking.MlflowClient()

model_name = 'AlperModel1'
version = '1.0.1'
description = 'desc 1.0.1'
tags = { "gitlab.version": version }

# Create a model
model = client.create_registered_model(model_name, description=description)

# Update a model
model = client.update_registered_model(model_name, description=description)

# Create a model version
client.create_model_version(model_name, version, description="version desc 1.0.1", tags=tags)

# Updating a model version
client.update_model_version(model_name, version, description=description, tags=tags)


# Uploading a file
model_version = client.get_model_version(model_name, version)
run_id = model_version.run_id
client.log_artifact(run_id, './models/stablecode-completion-alpha-3b-4k.ggmlv1.q4_0.bin', artifact_path="model")
