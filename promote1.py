import os
from mlflow import MlflowClient

os.environ["MLFLOW_TRACKING_URI"] = "http://gdk.test:3000/api/v4/projects/34/ml/mlflow/"
os.environ["MLFLOW_TRACKING_TOKEN"] = "glpat-redacted"

c = MlflowClient()
run = c.get_run('6441f108-07cd-4607-900e-44fd2cbbb8ce')
print(run)
c.log_artifact(run.info.run_id, "a.txt", artifact_path="features")
c.log_metric(run.info.run_id, "experiment", 33)
