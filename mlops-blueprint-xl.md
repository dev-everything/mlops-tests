---
title: "MLOps end-to-end Technical Blueprint"
status: proposed
creation-date: "2025-01-30"
authors: [ "@a_akgun", "@fdegier" ]
coach: "@igor.drozdov"
approvers: [ "@tmccaslin", "@sean_carrol" ]
owning-stage: "~devops::modelops"
participating-stages: []
toc_hide: true
---

{{< design-document-header >}}

This blueprint describes GitLab's end-to-end MLOps platform architecture, designed to support the complete machine learning lifecycle from experimentation to production deployment. This initiative supports our SaaS instance and self-managed instances  while maintaining our "single application" philosophy.

![image](/uploads/c016723a8a2de7fe0ba2b55a37003a34/image.png)

## Motivation

Organizations are struggling to productionize AI and machine learning (ML) solutions while managing the increasing complexity of infusing data science into their applications. Key challenges include reproducibility, automation, collaboration, scalability, and monitoring in ML processes. GitLab aims to provide a unified platform that bridges the gap between data science, engineering and governance teams.

### Goals

- Provide end-to-end ML lifecycle management integrated with existing development workflows
- Enable seamless collaboration between data scientists and engineering teams
- Support both traditional ML models and LLMs
- Maintain enterprise-grade security and compliance capabilities
- Reduce deployment times and improve model governance
- Enable integration with existing GitLab CI/CD pipelines
- Enable integration with existing GitLab features like issues, merge requests, tracing etc.
- Integrating with cloud providers; initially Sagemaker for training and deployment.
- Limited support for MLflow client for model experiments and registry
- Increase storage limits for Model Registry for Premium and Ultimate

### Non-Goals

- Hosting extensive computation resources for model training beyond GPU runners
- Providing a model serving infrastructure
- Implementing feature stores
- Implementing data stores
- Developing a full-fledged MLflow server
- Achieving 100% MLflow API compatibility

## Proposal

GitLab will provide a comprehensive MLOps platform built on top of existing GitLab infrastructure, leveraging and extending our CI/CD capabilities, package registry for artifact storage. The platform will support the full ML lifecycle through dedicated components while maintaining GitLab's single application philosophy.

## Design and Implementation Details

### Component Architecture

```mermaid
graph TB
    subgraph "Development Phase"
        A1[Experiment Tracking]
        A2[Model Registry]
        A3[GPU Runner Management]
        A4[Version Control]
    end

    subgraph "CI/CD Pipeline"
        B1[Model CI Pipeline]
        B2[Model Testing]
        B3[Model Validation]
        B4[Deployment Pipeline]
    end

    subgraph "Production Phase"
        C1[Model Serving]
        C2[Model Monitoring]
        C3[Model Governance]
        C4[Feedback Loop]
    end

    A1 --> A2
    A2 --> B1
    A3 --> A1
    A4 --> A1
    B1 --> B2
    B2 --> B3
    B3 --> B4
    B4 --> C1
    C1 --> C2
    C2 --> C3
    C3 --> C4
    C4 --> A1
```

### Core Components

#### 1. Experiment Tracking

The experiment management system will track ML training runs and their parameters:

- Experiment tracking with metadata storage
- Metric logging and visualization
- Storing artifacts
- Compatibility with MLflow client

#### 2. Model Registry

Central repository for ML model management: [Model registry docs](https://docs.gitlab.com/ee/user/project/ml/model_registry/).

- Model versioning and tagging (link to [docs](https://docs.gitlab.com/ee/user/project/ml/model_registry/#model-versions-and-semantic-versioning))
- Model metadata and lineage tracking
- Model approval workflows
- Integration with CI/CD pipelines
- Access control and security policies
- Compatibility with MLflow client
- Standardized model cards
- Governance instruments

### 3. Efficient management of GPU resources:

Link to [GPU runners docs](https://docs.gitlab.com/ee/ci/runners/hosted_runners/gpu_enabled.html).

- Auto-scaling of GPU runners
- Cost optimization
- Queue management
- Resource monitoring

#### 4. Model Deployment

Automated model deployment pipeline:

- Container-based deployment
- Multi-variate testing support
- Canary deployments
- Rollback capabilities
- Environment management
- Integration with cloud providers

#### 5. Model Monitoring

Comprehensive model observability:

- Performance monitoring
- Data drift detection
- Model quality metrics
- Resource utilization tracking
- Custom alert definitions
- Tracing via OpenTelemetry and [GitLab Tracing](https://docs.gitlab.com/ee/development/tracing.html)

#### 6. API Clients

- Gitlab MLOps client for Python
- Limited MLflow client support
- Command-line (cURL) support
#### 7. Model governance

- RBAC for ML assets
- Audit trails
- Approval workflows
- Basic compliance reporting

### Integration Points

1. **GitLab CI/CD Integration**
   - Custom pipeline templates for ML workflows
   - Predefined variables for ML operations
   - ML-specific CI/CD stages

2. **Container Registry Integration**
   - Model artifact storage
   - Version tagging
   - Security scanning

3. **Issue Tracking Integration**
   - Model development issues
   - Retraining triggers
   - Approval workflows

### Security and Compliance

1. **Access Control**
   - Role-based access control (RBAC)
   - Model-level permissions
   - Environment-based restrictions

2. **Audit Trail**
   - Model lineage tracking
   - Access logs
   - Change history
   - Compliance reporting

3. **Data Privacy**
   - Data encryption
   - PII detection
   - Access logging

### Deployment Options

MLOps will support self-managed installation, including support for air-gapped environments and GitLab.com deployment and GitLab Dedicated.

### Development Guidelines

No additional need beyond GDK. You might need MLflow client and [GitLab MLOps Python Client](https://pypi.org/project/gitlab-mlops/)

### Documentation

Comprehensive user, API and operations documentation will be provided:

   - Troubleshooting guides

## Out of scope

- Full MLflow client compatibility

## Conclusion

This technical blueprint provides a framework for implementing a comprehensive MLOps platform within GitLab. The proposed architecture leverages GitLab's existing strengths while adding ML-specific capabilities that enable organizations to effectively manage their ML workflows at scale.
